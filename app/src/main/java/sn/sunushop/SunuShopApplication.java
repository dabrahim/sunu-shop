package sn.sunushop;

import android.app.Application;

import dagger.hilt.android.HiltAndroidApp;

@HiltAndroidApp
public class SunuShopApplication extends Application {
}
