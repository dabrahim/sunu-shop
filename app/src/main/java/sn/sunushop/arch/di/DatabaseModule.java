package sn.sunushop.arch.di;

import android.content.Context;

import androidx.room.Room;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.qualifiers.ApplicationContext;
import dagger.hilt.components.SingletonComponent;
import sn.sunushop.arch.data.source.local.SunuShopLocalDatabase;

@Module
@InstallIn(SingletonComponent.class)
public class DatabaseModule {

    private static final String DATABASE_NAME = "SunuShop.db";

    @Provides
    public SunuShopLocalDatabase provideLocalDatabase(@ApplicationContext Context context) {
        return Room.databaseBuilder(context.getApplicationContext(), SunuShopLocalDatabase.class, DATABASE_NAME)
                .fallbackToDestructiveMigration()
                .build();
    }
}
