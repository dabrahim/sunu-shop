package sn.sunushop.arch.data;

import androidx.lifecycle.LiveData;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import sn.sunushop.arch.data.source.Product;

public class AppRepository {

    private final DatabaseService databaseService;

    @Inject
    public AppRepository(DatabaseService databaseService) {
        this.databaseService = databaseService;
    }

    public Observable<Long[]> insertAllProducts(Product... products) {
        return databaseService.insertAllProducts(products)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());
    }

    public Observable<Long> createProduct(Product product) {
        return databaseService.insert(product)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());
    }

    public LiveData<List<Product>> getAllProducts() {
        return databaseService.getAllProducts();
    }

    public Observable<Long> deleteProduct(Product product) {
        return databaseService.deleteProduct(product)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public LiveData<Product> findById(Long id) {
        return databaseService.findById(id);
    }

    public Observable<Long> updateProduct(Product product) {
        return databaseService.updateProduct(product)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
