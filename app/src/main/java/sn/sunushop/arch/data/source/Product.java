package sn.sunushop.arch.data.source;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Product {
    @PrimaryKey(autoGenerate = true)
    private long id;
    private String title;
    private String description;
    private long price;
    private boolean inStock;

    public Product() {}

    public Product(String title, String description, long price, boolean inStock) {
        this.title = title;
        this.description = description;
        this.price = price;
        this.inStock = inStock;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isInStock() {
        return inStock;
    }

    public void setInStock(boolean inStock) {
        this.inStock = inStock;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean equals(Product product) {
        return product.title.equals(title) &&
                product.description.equals(description) &&
                product.price == price &&
                product.inStock == inStock;
    }
}
