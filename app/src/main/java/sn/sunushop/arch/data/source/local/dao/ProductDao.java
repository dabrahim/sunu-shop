package sn.sunushop.arch.data.source.local.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import sn.sunushop.arch.data.source.Product;

@Dao
public interface ProductDao {
    @Insert
    long create(Product product);

    @Insert
    Long[] createAll(Product... products);

    @Query("SELECT * FROM product")
    LiveData<List<Product>> getAll();

    @Delete
    void delete(Product product);

    @Query("SELECT * FROM product WHERE id = :id")
    LiveData<Product> findById(long id);

    @Update
    void update(Product product);
}
