package sn.sunushop.arch.data;

import androidx.lifecycle.LiveData;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.rxjava3.core.Observable;
import sn.sunushop.arch.data.source.Product;
import sn.sunushop.arch.data.source.local.SunuShopLocalDatabase;

public class DatabaseService {

    public static final Long SUCCESS_VALUE = 1L;

    private final SunuShopLocalDatabase localDatabase;

    @Inject
    public DatabaseService(SunuShopLocalDatabase localDatabase) {
        this.localDatabase = localDatabase;
    }

    public Observable<Long[]> insertAllProducts(Product... products) {
        return Observable.fromCallable(() -> localDatabase.productDao().createAll(products));
    }

    public LiveData<List<Product>> getAllProducts() {
        return localDatabase.productDao().getAll();
    }

    public Observable<Long> insert(Product product) {
        return Observable.fromCallable(() -> localDatabase.productDao().create(product));
    }

    public Observable<Long> deleteProduct(Product product) {
        return Observable.fromCallable(() -> {
            localDatabase.productDao().delete(product);
            return null;
        });
    }

    public LiveData<Product> findById(Long id) {
        return localDatabase.productDao().findById(id);
    }

    public Observable<Long> updateProduct(Product product) {
        return Observable.fromCallable((() -> {
            localDatabase.productDao().update(product);
            return SUCCESS_VALUE;
        }));
    }
}
