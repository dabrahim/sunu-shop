package sn.sunushop.arch.data.source.local;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import sn.sunushop.arch.data.source.Product;
import sn.sunushop.arch.data.source.local.dao.ProductDao;

@Database(entities = {Product.class}, version = 2)
public abstract class SunuShopLocalDatabase extends RoomDatabase {
    public abstract ProductDao productDao();
}