package sn.sunushop.ui.adapter;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import sn.sunushop.R;
import sn.sunushop.arch.data.source.Product;
import sn.sunushop.databinding.ProductItemBinding;
import sn.sunushop.ui.adapter.ProductAdapter.ProductViewHolder;
import sn.sunushop.ui.screen.details.ProductDetailsFragment;

public class ProductAdapter extends ListAdapter<Product, ProductViewHolder> {

    public ProductAdapter() {
        this(false);
    }

    public ProductAdapter(boolean showControls) {
        super(new ProductDiffCallback());
        this.showControls = showControls;
    }

    private final boolean showControls;

    public interface OnControlButtonClicked {
        void onDelete(Product product);

        void update(Long id);
    }

    public interface OnMenuClickedListener {
        void onClick(View anchor, Long productId);
    }

    public static OnMenuClickedListener createMenuListener(FragmentActivity context, @IdRes int actionId) {
        return (anchor, productId) -> {
            PopupMenu popupMenu = new PopupMenu(context, anchor);
            popupMenu.inflate(R.menu.product_menu);

            popupMenu.setOnMenuItemClickListener(menuItem -> {
                final int itemId = menuItem.getItemId();

                if (itemId == R.id.details) {
                    Bundle bundle = new Bundle();
                    bundle.putLong(ProductDetailsFragment.PRODUCT_ID_KEY, productId);

                    Navigation.findNavController(context, R.id.nav_host)
                            .navigate(actionId, bundle);

                    return true;
                }

                return false;
            });

            popupMenu.show();
        };
    }

    private OnMenuClickedListener onMenuClickedListener;

    public void setOnMenuClickedListener(OnMenuClickedListener onMenuClickedListener) {
        this.onMenuClickedListener = onMenuClickedListener;
    }

    public void setOnClickListener(OnControlButtonClicked listener) {
        this.listener = listener;
    }

    private OnControlButtonClicked listener;

    static class ProductViewHolder extends RecyclerView.ViewHolder {

        private final ProductItemBinding binding;

        public ProductViewHolder(ProductItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void bind(Product product, OnControlButtonClicked onControlButtonClicked, boolean showControls, OnMenuClickedListener onMenuClickedListener) {
            binding.titleText.setText(product.getTitle());
            binding.descriptionText.setText(product.getDescription());
            binding.priceText.setText(product.getPrice() + " F.CFA");
            binding.inStockText.setText(product.isInStock() ? "In Stock" : "Not in stock");

            if (onControlButtonClicked != null) {
                binding.deleteIcon.setOnClickListener(v -> onControlButtonClicked.onDelete(product));
                binding.editIcon.setOnClickListener(v -> onControlButtonClicked.update(product.getId()));
            }

            if (showControls) {
                binding.editIcon.setVisibility(View.VISIBLE);
                binding.deleteIcon.setVisibility(View.VISIBLE);
            }

            if (onMenuClickedListener != null) {
                binding.showMenu.setOnClickListener(view -> onMenuClickedListener.onClick(view, product.getId()));
            }
        }

        static ProductViewHolder from(ViewGroup parent) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            ProductItemBinding binding = ProductItemBinding.inflate(layoutInflater, parent, false);

            return new ProductViewHolder(binding);
        }
    }

    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return ProductViewHolder.from(parent);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder holder, int position) {
        final Product product = getItem(position);
        holder.bind(product, this.listener, showControls, onMenuClickedListener);
    }
}

class ProductDiffCallback extends DiffUtil.ItemCallback<Product> {
    @Override
    public boolean areItemsTheSame(@NonNull Product oldItem, @NonNull Product newItem) {
        return oldItem.getId() == newItem.getId();
    }

    @Override
    public boolean areContentsTheSame(@NonNull Product oldItem, @NonNull Product newItem) {
        return oldItem.equals(newItem);
    }
}
