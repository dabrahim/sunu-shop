package sn.sunushop.ui.utility;

public class Event<T> {

    private final T content;

    private boolean dispatched = false;

    public Event(T content) {
        this.content = content;
    }

    public T getContentIfNotDispatchedOrReturnNull() {
        if (dispatched) {
            return null;
        } else {
            dispatched = true;
            return content;
        }
    }

    /**
     * Returns the content, even if it's already been handled.
     */
    public T peekContent() {
        return content;
    }
}
