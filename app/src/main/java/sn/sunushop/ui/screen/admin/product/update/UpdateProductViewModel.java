package sn.sunushop.ui.screen.admin.product.update;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.SavedStateHandle;
import androidx.lifecycle.ViewModel;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;
import sn.sunushop.arch.data.AppRepository;
import sn.sunushop.arch.data.source.Product;
import sn.sunushop.ui.utility.Event;
import sn.sunushop.ui.utility.ObservableWrapper;

@HiltViewModel
class UpdateProductViewModel extends ViewModel {

    private final AppRepository repository;
    private final LiveData<Product> productLiveData;

    private final MutableLiveData<Event<Boolean>> updateFinishedEvent = new MutableLiveData<>();

    public LiveData<Event<Boolean>> getUpdateFinishedEvent() {
        return updateFinishedEvent;
    }

    @Inject
    public UpdateProductViewModel(AppRepository repository, SavedStateHandle savedStateHandle) {
        this.repository = repository;
        Long productId = savedStateHandle.get("productId");
        productLiveData = repository.findById(productId);
    }

    public LiveData<Product> getProductLiveData() {
        return productLiveData;
    }

    public void onUpdate(Product product) {
        this.repository.updateProduct(product)
                .subscribe(new ObservableWrapper<Long>() {
                    @Override
                    public void onSuccess(Long aLong) {
                        updateFinishedEvent.setValue(new Event<>(true));
                    }

                    @Override
                    public void onFailure(Throwable e) {
                        updateFinishedEvent.setValue(new Event<>(false));
                        Log.e("__TAG__", e.getMessage(), e);
                    }
                });
    }
}
