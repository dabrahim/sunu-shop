package sn.sunushop.ui.screen.admin.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import java.util.List;

import dagger.hilt.android.AndroidEntryPoint;
import sn.sunushop.R;
import sn.sunushop.arch.data.source.Product;
import sn.sunushop.databinding.FragmentAdminHomeBinding;
import sn.sunushop.ui.adapter.ProductAdapter;

@AndroidEntryPoint
public class AdminHomeFragment extends Fragment {

    private FragmentAdminHomeBinding binding;

    private ProductAdapter adapter;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentAdminHomeBinding.inflate(getLayoutInflater(), container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        AdminHomeViewModel viewModel = new ViewModelProvider(this).get(AdminHomeViewModel.class);

        adapter = new ProductAdapter(true);

        adapter.setOnClickListener(new ProductAdapter.OnControlButtonClicked() {
            @Override
            public void onDelete(Product product) {
                viewModel.onDelete(product);
            }

            @Override
            public void update(Long id) {
                Bundle bundle = new Bundle();
                bundle.putLong("productId", id);

                Navigation.findNavController(requireActivity(), R.id.nav_host)
                        .navigate(R.id.action_adminHomeFragment_to_updateProductFragment, bundle);
            }
        });

        adapter.setOnMenuClickedListener(ProductAdapter.createMenuListener(requireActivity(), R.id.action_adminHomeFragment_to_productDetailsFragment));

        binding.recyclerView.setAdapter(adapter);

        // BINDING LIFECYCLE OBSERVERS
        final Observer<List<Product>> productsObserver = products -> adapter.submitList(products);
        viewModel.getProducts().observe(getViewLifecycleOwner(), productsObserver);

        // LISTENERS
        binding.fab.setOnClickListener(view1 -> {
            Navigation.findNavController(requireActivity(), R.id.nav_host)
                    .navigate(R.id.action_adminHomeFragment_to_createProductFragment);
        });
    }
}
