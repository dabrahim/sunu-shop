package sn.sunushop.ui.screen.admin.product.create;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;
import sn.sunushop.arch.data.AppRepository;
import sn.sunushop.arch.data.source.Product;
import sn.sunushop.ui.utility.Event;

@HiltViewModel
class CreateProductViewModel extends ViewModel {

    private final AppRepository repository;

    @Inject
    public CreateProductViewModel(AppRepository repository) {
        this.repository = repository;
    }

    private final MutableLiveData<Event<Boolean>> productCreatedEvent = new MutableLiveData<>();

    public LiveData<Event<Boolean>> getProductCreatedEvent() {
        return productCreatedEvent;
    }

    void onProductSubmitted(Product product) {
        repository.createProduct(product)
                .subscribe(new Observer<Long>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                    }

                    @Override
                    public void onNext(@NonNull Long aLong) {
                        productCreatedEvent.setValue(new Event<>(true));
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        productCreatedEvent.setValue(new Event<>(false));
                        Log.e("__TAG__", e.getMessage(), e);
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }
}
