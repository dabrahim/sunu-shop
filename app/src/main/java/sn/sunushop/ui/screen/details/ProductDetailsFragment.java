package sn.sunushop.ui.screen.details;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import dagger.hilt.android.AndroidEntryPoint;
import sn.sunushop.R;
import sn.sunushop.arch.data.source.Product;
import sn.sunushop.databinding.FragmentProductDetailsBinding;

@AndroidEntryPoint
public class ProductDetailsFragment extends Fragment {

    public static final String PRODUCT_ID_KEY = "productId";

    private FragmentProductDetailsBinding binding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentProductDetailsBinding.inflate(getLayoutInflater(), container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ProductDetailsViewModel viewModel = new ViewModelProvider(this).get(ProductDetailsViewModel.class);
        viewModel.getProductLiveData().observe(getViewLifecycleOwner(), this::bindData);
    }

    private void bindData(Product product) {
        binding.titleText.setText(product.getTitle());
        binding.descriptionText.setText(product.getDescription());
        binding.priceText.setText(getString(R.string.price, product.getPrice()));
    }
}
