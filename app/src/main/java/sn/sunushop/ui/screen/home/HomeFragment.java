package sn.sunushop.ui.screen.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import java.util.List;

import dagger.hilt.android.AndroidEntryPoint;
import sn.sunushop.R;
import sn.sunushop.arch.data.source.Product;
import sn.sunushop.databinding.FragmentHomeBinding;
import sn.sunushop.ui.adapter.ProductAdapter;

@AndroidEntryPoint
public class HomeFragment extends Fragment {

    private FragmentHomeBinding binding;

    private ProductAdapter adapter;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentHomeBinding.inflate(getLayoutInflater(), container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        HomeViewModel viewModel = new ViewModelProvider(this).get(HomeViewModel.class);

        adapter = new ProductAdapter();

        adapter.setOnMenuClickedListener(ProductAdapter.createMenuListener(requireActivity(), R.id.action_homeFragment_to_productDetailsFragment));

        binding.recyclerView.setAdapter(adapter);

        // BINDING LIFECYCLE OBSERVERS

        final Observer<List<Product>> productsObserver = products -> adapter.submitList(products);

        viewModel.getProducts().observe(getViewLifecycleOwner(), productsObserver);
    }
}
