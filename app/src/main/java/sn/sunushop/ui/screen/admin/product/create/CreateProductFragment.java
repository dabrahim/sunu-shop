package sn.sunushop.ui.screen.admin.product.create;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import dagger.hilt.android.AndroidEntryPoint;
import sn.sunushop.R;
import sn.sunushop.arch.data.source.Product;
import sn.sunushop.databinding.FragmentCreateProductBinding;

@AndroidEntryPoint
public class CreateProductFragment extends Fragment {

    private FragmentCreateProductBinding binding;
    private CreateProductViewModel viewModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentCreateProductBinding.inflate(getLayoutInflater(), container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = new ViewModelProvider(this).get(CreateProductViewModel.class);

        binding.createButton.setOnClickListener(v -> {
            Product product = new Product();

            product.setTitle(binding.titleEditText.getText().toString());
            product.setDescription(binding.descriptionEditText.getText().toString());
            product.setPrice(Long.parseLong(binding.priceEditText.getText().toString()));
            product.setInStock(binding.inStockSwitch.isChecked());

            viewModel.onProductSubmitted(product);
        });

        viewModel.getProductCreatedEvent().observe(getViewLifecycleOwner(), booleanEvent -> {
            if (booleanEvent.peekContent()) {
                // success
                Navigation.findNavController(requireActivity(), R.id.nav_host)
                        .navigate(R.id.action_createProductFragment_to_adminHomeFragment);
            } else {
                // failure
                Toast.makeText(getContext(), "An ERROR occurred. Please retry later.", Toast.LENGTH_LONG).show();
            }
        });
    }
}
