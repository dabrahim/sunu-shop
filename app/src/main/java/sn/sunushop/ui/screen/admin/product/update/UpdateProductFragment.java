package sn.sunushop.ui.screen.admin.product.update;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import dagger.hilt.android.AndroidEntryPoint;
import sn.sunushop.R;
import sn.sunushop.arch.data.source.Product;
import sn.sunushop.databinding.FragmentUpdateProductBinding;

@AndroidEntryPoint
public class UpdateProductFragment extends Fragment {

    private FragmentUpdateProductBinding binding;

    private UpdateProductViewModel viewModel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentUpdateProductBinding.inflate(getLayoutInflater(), container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel = new ViewModelProvider(this).get(UpdateProductViewModel.class);

        viewModel.getProductLiveData().observe(getViewLifecycleOwner(), this::bindData);

        binding.updateButton.setOnClickListener(v -> {
            Product product = new Product();

            product.setTitle(binding.titleEditText.getText().toString());
            product.setDescription(binding.descriptionEditText.getText().toString());
            product.setPrice(Long.parseLong(binding.priceEditText.getText().toString()));
            product.setInStock(binding.inStockSwitch.isChecked());
            product.setId(Long.parseLong(binding.idEditText.getText().toString()));

            viewModel.onUpdate(product);
        });

        viewModel.getUpdateFinishedEvent().observe(getViewLifecycleOwner(), booleanEvent -> {
            if (booleanEvent.peekContent()) {
                Navigation.findNavController(requireActivity(), R.id.nav_host)
                        .navigate(R.id.action_updateProductFragment_to_adminHomeFragment);

            } else {
                Toast.makeText(getContext(), "Couldn't update Product, please retry.", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void bindData(Product product) {
        binding.titleEditText.setText(product.getTitle());
        binding.descriptionEditText.setText(product.getDescription());
        binding.priceEditText.setText(String.valueOf(product.getPrice()));
        binding.inStockSwitch.setChecked(product.isInStock());
        binding.idEditText.setText(String.valueOf(product.getId()));
    }
}
