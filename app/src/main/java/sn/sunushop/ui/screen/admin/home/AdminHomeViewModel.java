package sn.sunushop.ui.screen.admin.home;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;
import sn.sunushop.arch.data.AppRepository;
import sn.sunushop.arch.data.source.Product;
import sn.sunushop.ui.utility.ObservableWrapper;

@HiltViewModel
public class AdminHomeViewModel extends ViewModel {

    private final AppRepository repository;

    private final LiveData<List<Product>> productsLiveData;

    @Inject
    public AdminHomeViewModel(AppRepository repository) {
        this.repository = repository;
        this.productsLiveData = repository.getAllProducts();
    }

    public LiveData<List<Product>> getProducts() {
        return this.productsLiveData;
    }

    public void onDelete(Product product) {
        repository.deleteProduct(product)
                .subscribe(new ObservableWrapper<Long>() {
                    @Override
                    public void onSuccess(Long aLong) {
                        Log.d("__TAG__", "Product deleted!");
                    }

                    @Override
                    public void onFailure(Throwable e) {
                        Log.e("__TAG__", e.getMessage(), e);
                    }
                });
    }

    public void onUpdate(Product product) {

    }
}
