package sn.sunushop.ui.screen.details;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.SavedStateHandle;
import androidx.lifecycle.ViewModel;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;
import sn.sunushop.arch.data.AppRepository;
import sn.sunushop.arch.data.source.Product;

@HiltViewModel
public class ProductDetailsViewModel extends ViewModel {

    private final LiveData<Product> productLiveData;

    @Inject
    public ProductDetailsViewModel(AppRepository repository, SavedStateHandle savedStateHandle) {
        final Long productId = savedStateHandle.get(ProductDetailsFragment.PRODUCT_ID_KEY);
        this.productLiveData = repository.findById(productId);
    }

    public LiveData<Product> getProductLiveData() {
        return productLiveData;
    }
}
