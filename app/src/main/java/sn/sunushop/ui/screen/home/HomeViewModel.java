package sn.sunushop.ui.screen.home;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;
import sn.sunushop.arch.data.AppRepository;
import sn.sunushop.arch.data.source.Product;

@HiltViewModel
public class HomeViewModel extends ViewModel {

    private final AppRepository repository;

    private final LiveData<List<Product>> productListLiveData;

    public LiveData<List<Product>> getProducts() {
        return productListLiveData;
    }

    @Inject
    public HomeViewModel(AppRepository repository) {
        this.repository = repository;
        productListLiveData = repository.getAllProducts();
    }

    /*public void init() {
        Product product1 = new Product("Glasses", "A beautiful pair of glasses.", 2500, false);
        Product product2 = new Product("HP Computer", "A nice HP computer", 2500, true);
        Product product3 = new Product("IPhone", "An expensive phone", 2500, true);
        Product product4 = new Product("LG Phone", "An LG Phone", 2500, true);
        Product product5 = new Product("Keyboard", "A keyboard", 5000, false);

        repository.insertAllProducts(product1, product2, product3, product4, product5)
                .subscribe(new Observer<Long[]>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                    }

                    @Override
                    public void onNext(Long @NonNull [] longs) {

                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }*/
}
